// console.log("Hello world");

// [Section] Arithmetic Operators

let x = 1397;
console.log("The value of x is" + x);
let y = 7831;
console.log("The value of y is" + y);

// Addition operator
let sum = x + y;
console.log("Result of addtion operator:" + sum);

// Difference Operator
let difference = x - y;
console.log("Result of subtraction operator:" + difference);

// Multiplication Operator
let product = x * y;
console.log("Result of multiplication:" + product);

// Division Operator
let quotient = x / y;
console.log("Result of Division:" + quotient);

// Modulo
let remainder = y % x;
console.log("Result of modulo:" + remainder);

let secondRemainder = x % y;
console.log("Result of modulo:" + secondRemainder);


// [Section] Assignment Operators
	// Basic assignment operator (=)
	// The assignment operator adds the value of the right operand to a variable and assigns the result to the value.

	let assignmentNumber = 8;
	console.log(assignmentNumber);

	// Addition Assignment Operator (+=)
	// The addition assignment operators adds the value of the right operand to a variable and assigns the result to the variable.

	// assignmentNumber = assignmentNumber + 2;
	// console.log(assignmentNumber);

	assignmentNumber += 2;
	console.log("Result of addition assignment operator :" +assignmentNumber);

	// Subtraction assignment operator (-=)
	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator :" +assignmentNumber);

	// Multiplication assignemnt operator (*=)

	assignmentNumber *= 4;
	console.log("Result of multiplication assignment operator :" +assignmentNumber);

	// Division assignemnt operator (/=)

	assignmentNumber /= 8;
	console.log("Result of division assignment operator :" +assignmentNumber);

// [Section] Multiple Operators and Parentheses
	/*
		1. 3*4 = 12
		2. 12/5 = 2.4
		3. 1+2 = 3
		4. 3-2.4 = 0.6
	*/

	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of MDAS rule:" + mdas);

	let pemdas = 1 + (2-3) * (4/5);
	console.log("Result of PEMDAS rule:" + pemdas);

// [Section] Incrementation and Decrement
	// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

	let z = 1;

	// Pre-increment
	let increment = ++z;
	console.log("Result of z in pre-increment: " + z);
	console.log("Result of increment in pre-increment:" + increment);

	// Post-increment
	increment = z++;
	console.log("Result of z in post-increment:" + z);
	console.log("Result of increment in post-increment:" + increment);
	increment = z++;
	console.log("Result of increment in post-increment" + increment);

	let p = 0;
	// Pre-decrement

	let decrement = --p;
	console.log("Result of p in pre-decrement:" + p);
	console.log("Result of decrement in pre-decrement:" + decrement);

	// Post-decrement
	decrement = p--;
	console.log("Result of z in post-decrement:" + p);
	console.log("Result of decrement in post-decrement:" + decrement);
	decrement = p--;
	console.log("Result of decrement in post-decrement:" + decrement);

// [Section] Type Coercion
	/*
		-Type coercion is the automatic or implicit conversion of values from one data type to another.
		-This happens when operations are performed on different data types that would normally not be possible and yield irregular results.
		-Values are automatically converted from one data type to another in order to resolve operations.
	*/

	let numA = '10';
	let numB = 12;
		/*
			-Adding or concatinating a string and a number will result into string.
			-This can be proven in the console by looking at the color of the text displayed.
		*/

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	// Non-coercion
	
	let numC = 16;
	let numD = 14;
	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);

	let numE = true + 1;
	console.log(numE);
	/*
		Type Coercion: Boolean value and number
		The result will be a number
		The boolean value will be converted. True = 1, False = 0
	*/

// [Section] Comparison Operators
	let juan = 'juan';

	// Equality Operator (==)
	/*
		-checks whether the operands are equal or have the same value
	*/

	let isEqual = 1 == 1;
	console.log(typeof isEqual);

	console.log(1 == 2);
	console.log(1 == '1');

	// Strict equality operator (===)
	console.log(1 === '1');
	console.log('juan' == 'juan');
	console.log('juan' == juan);

	// Inequality operator (!=)
	/*
		-checks whether the operands are not equal or have different content
		-attempts to convert and compare operands of different data types
	*/

	console.log(1 != 1);
	console.log(1 != 2);
	console.log(1 != '1');

	// Strict Inequality operator
	console.log(1 !== '1');

// [Section] Relational Operators
	// some comparison operators check whether one value is greater or less than to the other value.

	let a = 50;
	let b = 65;

	// GT or greater than operator (>)
	let isGreaterThan = a > b;
	console.log(isGreaterThan);

	// LT or less than operator (<)
	let isLessThan = a < b;
	console.log(isLessThan);

	// GTE or Greater than or Equal (>=)
	let isGTorEqual = a >= b;
	console.log(isGTorEqual);

	// LTE or Less than or Equal (<=)
	let isLTorEqual = a <= b;
	console.log(isLTorEqual);

	let numStr = "30";

	console.log(a > numStr);

	console.log(b <= numStr);

// [Section] Logical Operators

	let isLegalAge = true;
	let isRegistered = false;

	// Logical "And" Operator (&& - double ampersand)
	// returns true if all operand are true
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of Logical AND operator:" + allRequirementsMet);

	// Logical "OR" Operator (|| - double pipe)
	// returns true if one of the operand is are true
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of Logical OR operator:" + someRequirementsMet);

	// Logical "NOT" Operator (! - Exclamation Point)
	let someRequirementsNotMet = !isRegistered;
	console.log(someRequirementsNotMet);

